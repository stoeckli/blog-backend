import fs from "fs";
import nodemailer from "nodemailer";
//not working!
//import { google } from "googleapis";

//working
import googleapis from "googleapis";
const { google } = googleapis;
//import { result } from "lodash";
import { createBlogEntry, getWebToken } from "./controllers/todoController";
import { websiteUrl } from "./index";

let users = JSON.parse(fs.readFileSync("./users.json"));

const CLIENT_ID =
  "370725190802-bqqcst2q0r3revfjkk4ee2gqlo9gv2nn.apps.googleusercontent.com";
const CLIENT_SECRET = "rv_nrb_27Eu0r-aXm0TMu4We";
const REDIRECT_URI = "https://developers.google.com/oauthplayground";
const REFRESH_TOKEN =
  "1//04I-r2WuR5as5CgYIARAAGAQSNwF-L9IrYNBtABnU50Tijb2mOSP00wXNBlLOHmpLirynhXxvrHyP835AVyCEDeg9rwMDPoUe44o";

export const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);
oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

export async function sendMail(entry) {
  try {
    const accessToken = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: "cedric.stoeckli@gmail.com",
        clientId: CLIENT_ID,
        clientSecret: CLIENT_SECRET,
        refreshToken: REFRESH_TOKEN,
        accessToken: accessToken,
      },
    });
    let entryId = entry.id;

    const test = users.filter((entry) => {
      console.log(entry.email);
      const mailOptions = {
        from: "Cedric Stoeckli <cedric.stoeckli@gmail.com>",
        to: entry.email,
        subject: "Blog",
        text: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
        html: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
      };
      const result = transport.sendMail(mailOptions);
    });

    return result;
  } catch (error) {
    return error;
  }
}

// sendMail()
//   .then((result) => console.log("Email sent", result))
//   .catch((error) => console.log(error.message));

//export * from "../src/app";
