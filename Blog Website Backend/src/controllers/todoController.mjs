"use strict";

//import { time, timeLog } from "console";
import fs from "fs";
import Joi from "joi";
import jwt from "jsonwebtoken";
import { title } from "process";
//import date from "joi/lib/types/date";

import * as mail from "../mailer";

let BlogEntrys = JSON.parse(fs.readFileSync("./entries.json"));
let users = JSON.parse(fs.readFileSync("./users.json"));

export const getTodo = (req, res) => {
  res.send("respond with a resource");
};

export const getBlogEntries = (req, res) => {
  const { id, title, author } = req.query;
  console.log(`ID: ${id}`);
  console.log(`Title: ${title}`);
  console.log(`Author: ${author}`);
  let filteredEntries;
  let identificationNumbers;

  if (title) {
    filteredEntries = BlogEntrys.filter((entry) => entry.title === title);
  }

  if (author) {
    filteredEntries = BlogEntrys.filter((entry) => entry.author === author);
  }

  if (id) {
    filteredEntries = BlogEntrys.filter((entry) => entry.id === parseInt(id));
  }
  if (undefined !== filteredEntries && filteredEntries.length > 0) {
    res.json(filteredEntries);
  } else {
    res.json(BlogEntrys);
  }
};

export const getBlogEntry = (req, res) => {
  const blog = BlogEntrys.find((c) => c.id === parseInt(req.params.id));
  if (!blog) {
    return res.status(404).send("The blog with the given ID was not found");
  } else {
    res.send(blog.title + " <br> " + blog.content);
  }
};

export const getWebToken = (req, res) => {
  //test user
  const user = users[0];
  jwt.sign({ user: user }, "secretkey", (err, token) => {
    res.json({
      token: token,
    });
  });
};

export const createComment = (req, res) => {
  const blog = BlogEntrys.find((c) => c.id === parseInt(req.params.id));
  if (!blog) {
    return res.status(404).send("The blog with the given ID was not found");
  } else {
    const schema = {
      name: Joi.string().required(),
      comment: Joi.string().required().min(3),
    };

    const result = Joi.validate(req.body, schema);

    if (result.error) {
      return res.status(400).send(result.error.details[0].message);
    }
    const entry = {
      id: blog.comments.length + 1,
      name: req.body.name,
      comment: req.body.comment,
    };
    blog.comments.push(entry);
    console.log(blog);
    res.send(blog);
  }
};

export const createBlogEntry = (req, res) => {
  jwt.verify(req.token, "secretkey", (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      const schema = {
        //author: Joi.string().min(3).required(),
        title: Joi.string().min(3).required(),
        content: Joi.string().min(10).required(),
      };

      const result = Joi.validate(req.body, schema);

      if (result.error) {
        return res.status(400).send(result.error.details[0].message);
      }
      const entry = {
        id: BlogEntrys.length + 1,
        //author: req.body.author,
        author: authData.user.username,
        date: todaysDate(),
        title: req.body.title,
        content: req.body.content,
        comments: [],
        authentification: authData,
      };
      BlogEntrys.push(entry);
      res.send(entry);

      mail
        .sendMail(entry)
        .then((result) => console.log("Email sent", result))
        .catch((error) => console.log(error.message));

      // let data = []
      // let test = JSON.stringify(entry, null, 2);
      // data.push(test)
      // console.log(data)
      // fs.writeFileSync("./entries.json", test)
    }
  });
};

export const updateBlogEntry = (req, res) => {
  jwt.verify(req.token, "secretkey", (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      //Look up the course
      //If not existing, return 404
      const blog = BlogEntrys.find((c) => c.id === parseInt(req.params.id));
      if (!blog) {
        return res.status(404).send("The blog with the given ID was not found");
      }
      //Validate
      //If invalid, return 400 - Bad request
      console.log(`username: ${authData.user.username}`);
      console.log(`blog auhor: ${blog.author}`);
      if (authData.user.username === blog.author) {
        const schema = {
          //author: Joi.string().min(3),
          title: Joi.string().min(3),
          content: Joi.string().min(10),
        };
        const result = Joi.validate(req.body, schema);
        if (result.error) {
          return res.status(400).send(result.error.details[0].message);
        }
        //Update course
        //console.log(req.body.author);
        if (req.body.author != undefined) blog.author = authData.user.username;
        blog.date = todaysDate();
        //console.log(req.body.title);
        if (req.body.title != undefined) blog.title = req.body.title;
        if (req.body.content != undefined) blog.content = req.body.content;
        //Return updated course
        res.send(blog);
      } else {
        return res
          .status(404)
          .send("No permission to adapt a foreign blog entry");
        console.log(`username: ${authData.user.username}`);
        console.log(`blog auhor: ${blog.author}`);
      }
    }
  });
};

export const deleteBlogEntry = (req, res) => {
  jwt.verify(req.token, "secretkey", (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      //Look up the course
      //Not existing, return 404
      const blog = BlogEntrys.find((c) => c.id === parseInt(req.params.id));
      if (!blog) {
        return res
          .status(404)
          .send("The course with the given ID was not found");
      }
      if (authData.user.username === blog.author) {
        //Delete
        const index = BlogEntrys.indexOf(blog);
        BlogEntrys.splice(index, 1);
        //Return the same course
        res.send(blog);
      } else {
        console.log(`username: ${authData.user.username}`);
        console.log(`blog auhor: ${blog.author}`);
        return res
          .status(404)
          .send("No permission to delete a foreign blog entry");
      }
    }
  });
};

export const createTodo = (req, res) => {
  res.send("respond with a resource POST");
};

function todaysDate() {
  const dt = new Date();
  let month = "" + (dt.getMonth() + 1);
  let day = "" + dt.getDate();
  let year = dt.getFullYear();
  let hours = dt.getHours();
  let minutes = dt.getMinutes();
  let seconds = dt.getSeconds();

  month = (month < 10 ? "0" : "") + month;
  day = (day < 10 ? "0" : "") + day;

  let time = `${hours}:${minutes}:${seconds}`;
  let date = [year, month, day].join("-");
  return [date, time].join(" - ");
}

export const verifyToken = (req, res, next) => {
  //get auth header value
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
};
