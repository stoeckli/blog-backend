"use strict";

import express from "express";
import jwt from "jsonwebtoken";

import {
  getTodo,
  createTodo,
  getBlogEntries,
  getBlogEntry,
  createBlogEntry,
  updateBlogEntry,
  deleteBlogEntry,
  getWebToken,
  verifyToken,
  createComment,
} from "../controllers/todoController";

const router = express.Router();

//router.get("/blog?dateFrom=2021-01-01&dateTo=2021-06-01", getBlogEntries);
//router.get("/blog?blogerId=1", getBloggerEntries);

router.get("/blog", getBlogEntries);
router.get("/blog/:id", getBlogEntry);
router.post("/blog", verifyToken, createBlogEntry);
router.post("/blog/:id", createComment);
router.post("/login", getWebToken);
router.put("/blog/:id", verifyToken, updateBlogEntry);
router.delete("/blog/:id", verifyToken, deleteBlogEntry);

export default router;
