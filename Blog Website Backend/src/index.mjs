"use strict";

import express from "express";
import todoRouter from "./routes/todo.routes";
import jwt from "jsonwebtoken";

const app = express();
const port = 3000;
export const websiteUrl = `http://localhost:${port}`;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello");
});

app.listen(port, () => {
  console.log(`Blog listening at ${websiteUrl}`);
});

app.use("/", todoRouter);
